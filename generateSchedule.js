const axios = require("axios");
const cheerio = require("cheerio");
const moment = require("moment-timezone");
const fs = require("fs").promises;
const apiKey = process.env.F1_API_KEY;
const startYear = 2018;
const currentYear = new Date().getFullYear();

const sessionKeys = {
	conventional: [
		"p1",
		"p2",
		"p3",
		"q",
		"r"
	],
	sprint: [
		"p1",
		"q",
		"p2",
		"s",
		"r"
	]
};

const fetchMeetingsByIds = async (meetingIds) => {
	let meetingPromises = meetingIds.map(async meetingId => {
		return (
			await axios.get(`https://api.formula1.com/v1/editorial-assemblies/races?meeting=${meetingId}`,
				{
					headers: {
						apikey: apiKey,
						locale: "en"
					}
				}
			)
		).data;
	});

	return await Promise.all(meetingPromises);
};

const fetchMeetingIdsFromAPI = async () => {
	const response = (await axios.get("https://api.formula1.com/v1/editorial-eventlisting/events", {
		headers: {
			apikey: apiKey,
			locale: "en"
		}
	})).data;

	return response.events.map(event => event.meetingKey);
};

// The meeting ID/Key of each event of the year is included in the HTML of the schedule page
const fetchMeetingIdsFromWebsite = async (year) => {
	const response = (await axios.get(`https://www.formula1.com/en/racing/${year}.html`)).data;

	let $ = cheerio.load(response);
	let eventItems = $(".event-item-link");
	let meetingIds = [];

	eventItems.each((_, item) => {
		let meetingId = $(item).attr("data-meetingkey");

		if (meetingId)
			meetingIds.push(meetingId);
	});

	return meetingIds;
};

(async () => {
	if (!apiKey) {
		console.log("Missing API key!");
		process.exit(1);
	}

	for (let year = startYear; year <= currentYear; year++) {
		let meetingIds;

		// As far as I can tell from the endpoints I could find,
		// the events API endpoint only supports the current season
		if (year === currentYear)
			meetingIds = await fetchMeetingIdsFromAPI();
		else
			meetingIds = await fetchMeetingIdsFromWebsite(year);

		let meetings = await fetchMeetingsByIds(meetingIds);

		let schedule = meetings.map(meeting => {
			let roundNumber = Number(meeting.race.meetingNumber);

			// Best way I could find to determine is an event is testing
			if (meeting.assemblyVideoListByTag.tags.find(x => x.id === "F1:Topic/Testing"))
				roundNumber = 0;

			let sessions = [];
			let eventSessionKeys;

			// If "s" property exists, event is using sprint format
			let sprintWeekend = !!meeting.race.s;

			eventSessionKeys = sprintWeekend ? sessionKeys.sprint : sessionKeys.conventional;

			for (let sessionKey of eventSessionKeys) {
				let session = meeting.race[sessionKey];

				if (!session)
					continue;

				// For some reason F1 decide to set the timestamp in local time instead of UTC,
				// which makes everything a pain, so convert to UTC
				let sessionStartDate = moment(session.startTime + session.gmtOffset);

				sessions.push({
					name: session.description,
					startDate: sessionStartDate.utc().toISOString()
				});
			}

			return {
				roundNumber,
				meetingId: meeting.fomRaceId,
				country: meeting.race.meetingCountryName,
				location: meeting.race.meetingLocation,
				officialEventName: meeting.race.meetingOfficialName,
				eventName: meeting.race.meetingName,
				eventFormat: sprintWeekend ? "sprint" : "conventional",
				eventStartDate: meeting.race.meetingStartDate,
				eventEndDate: meeting.race.meetingEndDate,
				sessions,
				track: {
					officialName: meeting.race.trackStats.circuitOfficialName,
					direction: meeting.race.trackStats.direction,
					circuitType: meeting.race.trackStats.circuitType,
					trackLength: Number(meeting.race.trackStats.trackLength)
				},
				images: {
					circuitSmall: meeting?.circuitSmallImage?.url,
					circuitMedium: meeting?.circuitMediumImage?.url,
					circuitMap: meeting?.circuitMapImage?.url,
					race: meeting?.raceImage?.url,
					countryFlag: meeting?.raceCountryFlag?.url
				}
			};
		});

		await fs.writeFile(`./schedules/${year}.json`, JSON.stringify(schedule, null, 4));

		console.log(`Written ${meetings.length} events to ${year}.json`);
	}
})();
